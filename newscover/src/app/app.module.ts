import { UserRegistrationComponent } from './login/user-registration/user-registration.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MainLoginComponent } from './login/main-login/main-login.component';
import { DashboardComponent } from './main-components/dashboard/dashboard.component';
import { NewsSourcesComponent } from './main-components/news-sources/news-sources.component';
import { NewsSourceComponent } from './main-components/news-source/news-source.component';
import { CategoriesComponent } from './main-components/categories/categories.component';
import { AddCategoriesComponent } from './main-components/add-categories/add-categories.component';

@NgModule({
  declarations: [
    AppComponent,
    MainLoginComponent,
    UserRegistrationComponent,
    DashboardComponent,
    NewsSourcesComponent,
    NewsSourceComponent,
    CategoriesComponent,
    AddCategoriesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
